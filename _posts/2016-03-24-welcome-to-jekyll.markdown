---
layout: single-post
title:  "Pitch deck"
date:   2016-03-24 15:32:14 -0300
categories: main
---
This website describes SorceryAlg project. It contains a proposition for investors and AI companies.

There's one person behind the project at the moment, but I'm going to use "we" pronoun on this page.

Table of contents:
1. What problem do we aim to solve?
2. How are we going to accomplish it?
3. How are we going to make money?
4. What is the current progress?
5. What is the proposition for investors?
6. Who is behind the project?

# What problem do we aim to solve?

We aim to make it possible to create better and/or cheaper AI.

# How are we going to accomplish it?

We have a set of ideas that together constitute an algorithm for general intelligence. That algorithm can be applied to any task, e.g. playing a game like checkers or predicting the next word in a text. This algorithm might turn out to be more efficient than existing algorithm (in terms of how much computational power and data it needs), and that allows to create a better and/or cheaper AI.

Even, if it's not more efficient overall, it might be better at something (for example, it might be better at some tasks, it might be better at being fine-tuned etc.).

Some ideas might also work with neural networks, so even if my algorithm is significantly less efficient than the standard approach (neural network + backpropagation algorithm), then a neural network can be trained with a backpropagation algorithm and then my ideas can be used to make it more efficient, for example. The point is that, it doesn't have to be either this or that (my algorithm or existing approach), because it probably can be connected somehow.

We choose to keep the ideas secret, because once we disclose the ideas, we lose the bargaining power, and we won't be able to ensure that we're going to be financially rewarded for our contribution. And not being financially rewarded also stops us from being able to contribute more value, because making impact requires resources.

# How are we going to make money?

There are a few options, some are better, some are worse... If you have a better idea how this can be monetized, then please let us know.

## Plan A

We are going to sell the intellectual property (the algorithm and possibly a method for training it) to someone who is in the better position to make use of it (i.e. has the resources to train powerful AI).

We are going to demonstrate the progress, for example by sharing videos of AI that is based on our algorithm solving tasks or playing games, or by releasing the solution to a task (the program that the algorithm has learned), or simply by describing the results. The video or a solution will demonstrate the breakthrough, but it won't necessarily completely prove the breakthrough, so we would be willing to sign a contract with the buyer that in case of our misrepresentation, we will pay some cost (so that the buyer can be certain that we haven't misrepresented anything, when demonstrating the progress).

## Plan B

We are going to fund raise a lot of money and train powerful AI model by ourselves. We are going to monetize the AI model (e.g. through an API).

Before that, we are going to demonstrate the progress as described in the plan A.

## Plan C

We are going to make a system where people can contribute their computational power in exchange for a stake in the company (in some way) and that computational power will be used to train a powerful AI model (e.g. that will be exposed through an API).

Before that, we are going to demonstrate the progress as described in the plan A.

## Plan D

We are going to make a provisional patent application(s) and then openly share the intellectual property. We are going to demonstrate the progress as described in the plan A. Then, we are going to make a non-provisional patent, and then make money by selling the licence to use the algorithm for commercial purposes.

## Plan E

We are going to sell the ideas (that are behind the algorithm) before they are implemented. The problem with selling non-implemented ideas is that the buyer can't know their value, until the idea is disclosed. But when the seller discloses the idea, they lose the bargaining power (i.e. the buyer doesn't have any incentive to pay the seller for the idea, once they buyer knows the idea). The problem can be solved by sharing the ideas in packets as follows (unless there is some problem with this approach that I can't see). The seller discloses one idea (or part of an idea), then buyer pays for it as much they think it's worth it. If the seller is happy with the reward for the first idea, then the seller discloses the next idea (or part of an idea), then the buyer pays for that. The process continues until all ideas are disclosed. The buyer has the incentive to reward the seller properly at each step, because if they won't, the next ideas will not be disclosed to them. The seller will therefore be rewarded properly.

# What is the current progress?

## Ideation

We have a vision of an algorithm for general intelligence. We expect that the vision will evolve while we are implementing the vision. We expect that we will be able to demonstrate significant breakthrough after from 3 months to 3 years (if the project is continued).

## Execution

We have implemented a small part of our algorithm, apply it to play FrozenLake game (a simple text game), and compared the results to the results achieved when you take the random actions in the game. When you take the random actions in the game, then the game ends up with a win about 1.5% times on average. When using our algorithm, after it was trained to play the game (for about 30 seconds), the game ends up with a win about 55-95% of times (depending of how successful the training was).

# What is the proposition to investors?

The proposition is to invest a small amount of money and/or commit to invest a bigger amount of money later, when more progress is demonstrated.

The small amount of money would allow us to demonstrate more progress, after which we would expect more money to be invested, after which we would be able to demonstrate more progress and so on...

We have described the current progress above, but not proved it. For that reason, we would be willing to sign a contract that states that if the investor doesn't see the return of the investment for long enough, then we are obliged to prove the progress that has been claimed in the moment of the investment (by sharing the code that achieves the result claimed in the moment of the investment). If we misrepresented anything about the progress, we will pay a penalty. This removes the need for the investor to trust us about the current progress.

# What is the proposition to AI companies?

The proposition is to buy the ideas and the code containing the implementation of the ideas.

One way how that could work was described in "Plan E" of "How are we going to make money", but we are open also for other ways.

# Who is behind the project?

My name is Damian. Here you can learn more about me: [https://damc4.gitlab.io](https://damc4.gitlab.io).

I'm open for collaboration and more people joining the project.

I'm using "we" in this text, instead of "I", because the execution of the plan would probably require a team at some point.
